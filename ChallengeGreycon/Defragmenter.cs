﻿using DefragmenterApp.Models;
using System;
using System.Linq;

namespace DefragmenterApp
{
    public class Defragmenter
    {
        private DisksBundle Disks { get; set; }

        public Defragmenter() { }

        public int Defrag(DisksBundle disks)
        {
            this.Disks = disks;

            int totalUsed = this.Disks.Used.Sum();
            int[] sortedTotals = this.Disks.Total.OrderByDescending(c => c).ToArray();
            int disksNeeded = 0;

            foreach (int total in sortedTotals)
            {
                //The disks are filled according to the capacity in a descending way
                totalUsed -= total;
                disksNeeded++;

                //If the remainder is negative, it means that the disk received the end of the elements 
                //and has free space
                if (totalUsed <= 0)
                    break;
            }

            return disksNeeded;
        }

        public static void Main()
        {
            DisksBundle testDisks0 = new DisksBundle()
            {
                Used = new int[] { 300, 525, 110 },
                Total = new int[] { 350, 600, 115 }
            };

            Defragmenter defragmenter = new Defragmenter();
            int disksNeeded = defragmenter.Defrag(testDisks0);

            Console.Out.WriteLine($"Answer: {disksNeeded}");
            Console.ReadKey();
        }
    }
}
