﻿namespace DefragmenterApp.Models
{
    public class DisksBundle
    {
        public int[] Used { get; set; }
        public int[] Total { get; set; }
    }
}
