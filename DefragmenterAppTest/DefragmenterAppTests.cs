﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DefragmenterApp;
using DefragmenterApp.Models;

namespace DefragmenterAppTest
{
    [TestClass]
    public class DefragmenterAppTests
    {
        [TestMethod]
        public void ExampleN0()
        {
            DisksBundle testDisks0 = new DisksBundle()
            {
                Used = new int[] { 300, 525, 110 },
                Total = new int[] { 350, 600, 115 }
            };

            Defragmenter defragmenter = new Defragmenter();
            Assert.AreEqual(2, defragmenter.Defrag(testDisks0), "Wrong number of discs required");
        }

        [TestMethod]
        public void ExampleN1()
        {
            DisksBundle testDisks1 = new DisksBundle()
            {
                Used = new int[] { 1, 200, 200, 199, 200, 200 },
                Total = new int[] { 1000, 200, 200, 200, 200, 200 }
            };

            Defragmenter defragmenter = new Defragmenter();
            Assert.AreEqual(1, defragmenter.Defrag(testDisks1), "Wrong number of discs required");
        }

        [TestMethod]
        public void ExampleN2()
        {
            DisksBundle testDisks2 = new DisksBundle()
            {
                Used = new int[] { 750, 800, 850, 900, 950 },
                Total = new int[] { 800, 850, 900, 950, 1000 }
            };

            Defragmenter defragmenter = new Defragmenter();
            Assert.AreEqual(5, defragmenter.Defrag(testDisks2), "Wrong number of discs required");
        }

        [TestMethod]
        public void ExampleN3()
        {
            DisksBundle testDisks3 = new DisksBundle()
            {
                Used = new int[] { 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49 },
                Total = new int[] { 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50 }
            };

            Defragmenter defragmenter = new Defragmenter();
            Assert.AreEqual(49, defragmenter.Defrag(testDisks3), "Wrong number of discs required");
        }

        [TestMethod]
        public void ExampleN4()
        {
            DisksBundle testDisks4 = new DisksBundle()
            {
                Used = new int[] { 331, 242, 384, 366, 428, 114, 145, 89, 381, 170, 329, 190, 482, 246, 2, 38, 220, 290, 402, 385 },
                Total = new int[] { 992, 509, 997, 946, 976, 873, 771, 565, 693, 714, 755, 878, 897, 789, 969, 727, 765, 521, 961, 906 }
            };

            Defragmenter defragmenter = new Defragmenter();
            Assert.AreEqual(6, defragmenter.Defrag(testDisks4), "Wrong number of discs required");
        }
    }
}
